package com.powerapps.hestories.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.powerapps.hestories.R;
import com.powerapps.hestories.model.StoryModel;

import java.io.IOException;
import java.util.ArrayList;

public class Splash extends Activity {

    private final int SPLASH_DISPLAY_LENGTH = 2500;
    private ArrayList<StoryModel> arrayListEnglish;
    private ArrayList<StoryModel> arrayListHindi;
    private StoryModel model;
    private Button btnRetry;
    private TextView tvWait;
    private ProgressBar progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.splashscreen);

        tvWait = findViewById(R.id.tv_wait);
        btnRetry = findViewById(R.id.btnRetry);
        progressBar = findViewById(R.id.progressbar);
        btnRetry.setVisibility(View.GONE);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                if (Utils.isNetworkAvailable(getApplicationContext())) {
                    setview_data();
                } else {
                    btnRetry.setVisibility(View.VISIBLE);
                    tvWait.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);
                }
            }
        }, 1000);

        btnRetry.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(getApplicationContext())) {
                    btnRetry.setVisibility(View.GONE);
                    tvWait.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.VISIBLE);
                    setview_data();
                } else {
                    btnRetry.setVisibility(View.VISIBLE);
                    tvWait.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);
                }
            }
        });
    }

    public void setview_data() {
        DataBaseHelper myDbHelper = new DataBaseHelper(getApplicationContext());
        try {
            myDbHelper.createDataBase();
            SQLiteDatabase myDB = myDbHelper.openDataBase();
            arrayListEnglish = new ArrayList<>();
            Cursor c = myDB.rawQuery("select * from story_table WHERE type= 'english'", null);
            if (c != null && c.getCount() > 0) {
                if (c.moveToFirst()) {
                    do {
                        String title = c.getString(c.getColumnIndex("title"));
                        String story = c.getString(c.getColumnIndex("story"));
                        String type = c.getString(c.getColumnIndex("type"));
                        model = new StoryModel();
                        model.setTitle(title);
                        model.setStory(story);
                        model.setType(type);
                        arrayListEnglish.add(model);
                    } while (c.moveToNext());
                }
            }
            c.close();
            StoryModel.setArrayListEnglish(arrayListEnglish);
            arrayListHindi = new ArrayList<>();
            Cursor c1 = myDB.rawQuery("select * from story_table WHERE type= 'hindi'", null);
            if (c1 != null && c1.getCount() > 0) {
                if (c1.moveToFirst()) {
                    do {
                        String title = c1.getString(c1.getColumnIndex("title"));
                        String story = c1.getString(c1.getColumnIndex("story"));
                        String type = c1.getString(c1.getColumnIndex("type"));
                        model = new StoryModel();
                        model.setTitle(title);
                        model.setStory(story);
                        model.setType(type);
                        arrayListHindi.add(model);
                    } while (c1.moveToNext());
                }
            }
            c1.close();
            myDbHelper.close();
            StoryModel.setArrayListHindi(arrayListHindi);
            mainpage();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void mainpage() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent mainIntent = new Intent(Splash.this, SelectLang.class);
                Splash.this.startActivity(mainIntent);
                Splash.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
