package com.powerapps.hestories.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.powerapps.hestories.R;
import com.powerapps.hestories.adapter.StoryAdapter;
import com.powerapps.hestories.model.StoryModel;

import java.util.ArrayList;

public class StoryListActivity extends AppCompatActivity {

    private ArrayList<StoryModel> arrayModel;
    private String type;
    private ListView listView;
    private StoryAdapter adapter;
    private InterstitialAd interstitial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.story_list);

        type = getIntent().getStringExtra("Type");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(type + " Stories");

        AdView adView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        interstitial = new InterstitialAd(StoryListActivity.this);
        interstitial.setAdUnitId(getString(R.string.interstital_ad_id));
        interstitial.loadAd(adRequest);
        interstitial.setAdListener(new AdListener() {
            public void onAdLoaded() {
                displayInterstitial();
            }
        });

        if (type.equalsIgnoreCase("english")) {
            arrayModel = StoryModel.getArrayListEnglish();
        } else if (type.equalsIgnoreCase("hindi")) {
            arrayModel = StoryModel.getArraListHindi();
        }
        listView = findViewById(R.id.list);
        adapter = new StoryAdapter(getApplicationContext(), arrayModel);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(StoryListActivity.this, StoryDetail.class);
                String title = arrayModel.get(position).getTitle();
                String story = arrayModel.get(position).getStory();
                intent.putExtra("Title", title);
                intent.putExtra("Story", story);
                startActivity(intent);
            }
        });
    }

    public void displayInterstitial() {
        if (interstitial.isLoaded()) {
            interstitial.show();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.share) {
            final String appPackageName = getPackageName();
            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Hindi Story App");
            sharingIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=" + appPackageName);
            startActivity(Intent.createChooser(sharingIntent, "Share Via"));
            return true;
        } else if (id == R.id.crickbetting) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=com.lexcorp.crickbettingpro")));
        }
        return super.onOptionsItemSelected(item);
    }
}
