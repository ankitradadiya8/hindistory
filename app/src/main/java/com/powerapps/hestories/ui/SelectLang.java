package com.powerapps.hestories.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.powerapps.hestories.R;
import com.powerapps.hestories.adapter.ImageAdapter;

public class SelectLang extends Activity {

    private Button btnEng, btnHindi;
    private GridView gridView;
    private String[] ORDER_ITEM = new String[]{"Bollywood", "News in Hindi",
            "CrickBetting"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_language);

        AdView adView = findViewById(R.id.adView);
        AdView adViewTop = findViewById(R.id.adView1);
        adView.loadAd(new AdRequest.Builder().build());
        adViewTop.loadAd(new AdRequest.Builder().build());

        gridView = findViewById(R.id.gridView);
        btnEng = findViewById(R.id.btnEnglish);
        btnHindi = findViewById(R.id.btnHindi);

        gridView.setAdapter(new ImageAdapter(getApplicationContext().getApplicationContext(), ORDER_ITEM));
        gridView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if (position == 0) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=com.lexcorp.bollywood")));
                } else if (position == 1) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=com.lexcorp.news")));
                } else if (position == 2) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=com.lexcorp.crickbetting")));
                }
            }
        });

        btnEng.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(SelectLang.this, StoryListActivity.class);
                i.putExtra("Type", "English");
                startActivity(i);
            }
        });

        btnHindi.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(SelectLang.this, StoryListActivity.class);
                i.putExtra("Type", "Hindi");
                startActivity(i);
            }
        });
    }
}
