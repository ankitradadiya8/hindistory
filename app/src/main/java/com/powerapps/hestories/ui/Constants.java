package com.powerapps.hestories.ui;

public interface Constants {

	/**
	 * Alert Dialog messages
	 */
	public final String MSG_WAIT = "Please wait...";
	public final String MSG_TITLE_ERROR = "Error";
	public final String MSG_TITLE_INFO = "Info";
	public final String MSG_TITLE_WARNING = "Warning";
	public final String MSG_TITLE_SUCCESS = "Success";
	public final String MSG_USER_NOT_ACTIVE = "User Not Active. Contact to Admin";
	public final String MSG_CONNECTION_ERROR = "Unable to access server. Please check your Internet Connection.";
	public final String MSG_BAD_RESPONSE = "Check your Internet Connection.";
	public final String MSG_NULL = "result is null";

}
