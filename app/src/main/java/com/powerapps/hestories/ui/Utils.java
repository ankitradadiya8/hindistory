package com.powerapps.hestories.ui;

import android.content.Context;
import android.net.ConnectivityManager;
import android.widget.Toast;

public class Utils {

	public static boolean isNetworkAvailable(Context mContext) {
		ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (cm != null && cm.getActiveNetworkInfo() != null) {
			return true;
		}
		Toast.makeText(mContext, "Please Open Internet Connection", Toast.LENGTH_SHORT).show();
		return false;
	}
}
