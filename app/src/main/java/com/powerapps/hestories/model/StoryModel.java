package com.powerapps.hestories.model;

import java.util.ArrayList;

import org.json.JSONObject;

public class StoryModel {

	private String title , story, type;
	public static ArrayList<StoryModel> arrayListEnglish = new ArrayList<>();
	public static ArrayList<StoryModel> arraListHindi = new ArrayList<>();

	public StoryModel(JSONObject jsonObject) {
		try {
			setTitle(jsonObject.getString("title"));
		} catch (Exception e) {
			setTitle(null);
		}
		
		try {
			setStory(jsonObject.getString("story"));
		} catch (Exception e) {
			setStory("");
		}
		
		try {
			setType(jsonObject.getString("type"));
		} catch (Exception e) {
			setType("");
		}
	}
	
	public StoryModel() {
		// TODO Auto-generated constructor stub
	}
	
	public static ArrayList<StoryModel> getArrayListEnglish() {
		return arrayListEnglish;
	}

	public static void setArrayListEnglish(ArrayList<StoryModel> arrayListEnglish) {
		StoryModel.arrayListEnglish = arrayListEnglish;
	}

	public static ArrayList<StoryModel> getArraListHindi() {
		return arraListHindi;
	}

	public static void setArrayListHindi(ArrayList<StoryModel> arraListHindi) {
		StoryModel.arraListHindi = arraListHindi;
	}

	public String getStory() {
		return story;
	}

	public void setStory(String story) {
		this.story = story;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
