package com.powerapps.hestories.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.powerapps.hestories.R;
import com.powerapps.hestories.model.StoryModel;

public class StoryAdapter extends BaseAdapter {

	Context context;
	ArrayList<StoryModel> values;

	public StoryAdapter(Context context, ArrayList<StoryModel> list) {
		this.context = context;
		this.values = list;
	}

	@Override
	public int getCount() {
		return values.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.story_list_item, null);
		} else {
			v = (View) convertView;
		}
		TextView title = (TextView) v.findViewById(R.id.tv_title);
		title.setText(values.get(position).getTitle());
		return v;
	}

}
