package com.powerapps.hestories.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.powerapps.hestories.R;

public class ImageAdapter extends BaseAdapter {

    private Context context;
    private final String[] orderValues;

    public ImageAdapter(Context context, String[] orderValues) {
        this.context = context;
        this.orderValues = orderValues;
    }

    @Override
    public int getCount() {
        return orderValues.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;
        if (convertView == null) {
            gridView = new View(context);
            gridView = inflater.inflate(R.layout.custom_grid_layout, null);
            TextView textView = (TextView) gridView.findViewById(R.id.lable);
            textView.setText(orderValues[position]);

            ImageView imageView = (ImageView) gridView.findViewById(R.id.icon);
            String order = orderValues[position];
            if (order.equals("Bollywood")) {
                imageView.setImageResource(R.drawable.bollywood);
            } else if (order.equals("News in Hindi")) {
                imageView.setImageResource(R.drawable.news);
            } else if (order.equals("CrickBetting")) {
                imageView.setImageResource(R.drawable.ballsm);
            }
        } else {
            gridView = (View) convertView;
        }
        return gridView;
    }

}
